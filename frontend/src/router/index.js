import Vue from "vue";
import Router from "vue-router";
import ListProductView from "@/components/Views/ListProductView";
import DetailsProductView from "@/components/Views/DetailsProductView";
import ComparisonProductView from "@/components/Views/ComparisonProductView";
import NewProductView from "@/components/Views/NewProductView";
import DeleteProductView from "@/components/Views/DeleteProductView";
import RegisterView from "@/components/Views/RegisterView";
import LoginView from "@/components/Views/LoginView";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "ListProductView",
      component: ListProductView
    },
    {
      path: "/item/:id",
      name: "DetailsProductView",
      component: DetailsProductView
    },
    {
      path: "/compare/:id1/:id2",
      name: "ComparisonProductView",
      component: ComparisonProductView
    },
    {
      path: "/newproduct/",
      name: "NewProductView",
      component: NewProductView
    },
    {
      path: "/deleteproduct/",
      name: "DeleteProductView",
      component: DeleteProductView
    },
    {
      path: "/register/",
      name: "RegisterView",
      component: RegisterView
    },
    {
      path: "/login/",
      name: "LoginView",
      component: LoginView
    }
  ]
});
