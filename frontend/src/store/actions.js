export default {
  addProduct({ commit }, product) {
    commit("addProduct", product);
  },
  deleteProduct({ commit }, product) {
    commit("deleteProduct", product);
  }
};
