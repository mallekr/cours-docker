export default {
  getProductById: (state) => (id) => {
    return state.products.find((p) => p.id === id);
  },
  getProducts(state) {
    return state.products;
  }
};
