import Vue from "vue";
import Vuex from "vuex";
import getters from "./getters.js";
import actions from "./actions.js";

import axios from "axios";

const API_BASE_URL = "https://nodejs-gray-seven.vercel.app/";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    products: []
  },
  mutations: {
    chargeProducts(state, products) {
      state.products = products;
    },
    addProduct(state, product) {
      state.products.push(product);
    },
    deleteProduct(state, productId) {
      // Appeler l'API pour supprimer le produit du backend
      axios
        .delete(`${API_BASE_URL}/delete/${productId}`)
        .then((response) => {
          // Gérer la réponse du serveur si besoin
        })
        .catch((error) => {
          // Gérer les erreurs éventuelles
        });
    }
  },
  getters,
  actions
});
